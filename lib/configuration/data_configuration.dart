class DataConfiguration {
  static const DATABASE_NAME = 'playd.db';
  static const DATABASE_VERSION = 1;
  static const CACHE_DURATION = 3600;

  String databaseName;
  int databaseVersion;
  int cacheDuration;

  DataConfiguration(
      {this.databaseName = DATABASE_NAME,
      this.databaseVersion = DATABASE_VERSION,
      this.cacheDuration = CACHE_DURATION});
}
