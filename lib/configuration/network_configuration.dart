import 'package:meta/meta.dart';

enum UriScheme { http, https }

class NetworkConfiguration {
  static const LOOKUP_DOMAIN = 'google.com';

  UriScheme scheme;
  String authority;
  String apiPrefix;
  String clientId;
  String clientSecret;
  String applicationIdentifier;
  String lookupDomain = LOOKUP_DOMAIN;

  NetworkConfiguration(
      {@required this.scheme,
      @required this.authority,
      @required this.apiPrefix,
      @required this.clientId,
      @required this.clientSecret,
      this.applicationIdentifier = '',
      this.lookupDomain = LOOKUP_DOMAIN});

  Uri getUri(String path,
      [Map<String, String> querystring, bool useApiPrefix = true]) {
    switch (this.scheme) {
      case UriScheme.http:
        return Uri.http(
            authority, ((useApiPrefix) ? apiPrefix : '') + path, querystring);
      case UriScheme.https:
        return Uri.https(
            authority, ((useApiPrefix) ? apiPrefix : '') + path, querystring);
      default:
        return null;
    }
  }
}
