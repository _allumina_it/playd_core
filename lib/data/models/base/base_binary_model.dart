import 'package:playd_core/data/models/base/base_model.dart';

abstract class BaseBinaryModel extends BaseModel {
  String slug;
  String name;
  String description;
  String mime;
  int size;

  BaseBinaryModel() : super();

  BaseBinaryModel.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.slug = map['slug'];
    this.name = map['name'];
    this.description = map['description'];
    this.mime = map['mime'];
    this.size = map['size'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['slug'] = this.slug;
    map['name'] = this.name;
    map['description'] = this.description;
    map['mime'] = this.mime;
    map['size'] = this.size;
    return map;
  }
}
