import 'package:playd_core/data/models/base/base_model.dart';

abstract class BaseContentModel extends BaseModel {
  String slug;
  String label;
  String title;
  String launch;
  String body;
  String scheduling;
  int startTime;
  int endTime;
  double latitude;
  double longitude;

  BaseContentModel() : super();

  BaseContentModel.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.slug = map['slug'];
    this.label = map['label'];
    this.title = map['title'];
    this.launch = map['launch'];
    this.body = map['body'];
    this.scheduling = map['scheduling'];
    this.startTime = map['start_time'];
    this.endTime = map['end_time'];
    this.latitude = map['latitude'];
    this.longitude = map['longitude'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['slug'] = this.slug;
    map['label'] = this.label;
    map['title'] = this.title;
    map['launch'] = this.launch;
    map['body'] = this.body;
    map['scheduling'] = this.scheduling;
    map['start_time'] = this.startTime;
    map['end_time'] = this.endTime;
    map['latitude'] = this.latitude;
    map['longitude'] = this.longitude;
    return map;
  }
}
