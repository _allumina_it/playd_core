import 'package:playd_core/data/models/base/base_model.dart';

abstract class BaseEventModel extends BaseModel {
  String name;
  String description;
  int timestamp;
  double latitude;
  double longitude;

  BaseEventModel() : super();

  BaseEventModel.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.name = map['name'];
    this.description = map['description'];
    this.timestamp = map['timestamp'];
    this.latitude = map['latitude'];
    this.longitude = map['longitude'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['name'] = this.name;
    map['description'] = this.description;
    map['timestamp'] = this.timestamp;
    map['latitude'] = this.latitude;
    map['longitude'] = this.longitude;
    return map;
  }
}
