import 'package:playd_core/data/models/base/base_model.dart';

abstract class BaseRawModel extends BaseModel {
  double value;
  double last;
  double min;
  double max;
  double average;
  double count;
  double trend;
  int day;
  int week;
  int month;
  int year;
  int unit;
  int interval;

  BaseRawModel() : super();

  BaseRawModel.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.value = map['value'];
    this.last = map['last'];
    this.min = map['min'];
    this.max = map['max'];
    this.average = map['average'];
    this.count = map['count'];
    this.trend = map['trend'];
    this.day = map['day'];
    this.week = map['week'];
    this.month = map['month'];
    this.year = map['year'];
    this.unit = map['unit'];
    this.interval = map['interval'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['value'] = this.value;
    map['last'] = this.last;
    map['min'] = this.min;
    map['max'] = this.max;
    map['average'] = this.average;
    map['count'] = this.count;
    map['trend'] = this.trend;
    map['day'] = this.day;
    map['week'] = this.week;
    map['month'] = this.month;
    map['year'] = this.year;
    map['unit'] = this.unit;
    map['interval'] = this.interval;
    return map;
  }
}
