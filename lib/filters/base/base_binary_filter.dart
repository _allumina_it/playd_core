import 'package:playd_core/filters/base/base_filter.dart';

abstract class BaseBinaryFilter extends BaseFilter {
  String slug;
  String name;
  String description;
  String mime;
  int size_min;
  int size_max;

  BaseBinaryFilter() : super();

  BaseBinaryFilter.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.slug = map['slug'];
    this.name = map['name'];
    this.description = map['description'];
    this.mime = map['mime'];
    this.size_min = map['size_min'];
    this.size_max = map['size_max'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['slug'] = this.slug;
    map['name'] = this.name;
    map['description'] = this.description;
    map['mime'] = this.mime;
    map['size_min'] = this.size_min;
    map['size_max'] = this.size_max;
    return map;
  }
}
