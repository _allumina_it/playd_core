import 'package:playd_core/filters/base/base_filter.dart';

abstract class BaseContentFilter extends BaseFilter {
  String slug;
  String label;
  String title;
  String launch;
  String body;
  String scheduling;
  int startTimeMin;
  int startTimeMax;
  int endTimeMin;
  int endTimeMax;
  double latitudeMin;
  double latitudeMax;
  double longitudeMin;
  double longitudeMax;

  BaseContentFilter() : super();

  BaseContentFilter.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.slug = map['slug'];
    this.label = map['label'];
    this.title = map['title'];
    this.launch = map['launch'];
    this.body = map['body'];
    this.scheduling = map['scheduling'];
    this.startTimeMin = map['start_time_min'];
    this.startTimeMax = map['start_time_max'];
    this.endTimeMin = map['end_time_min'];
    this.endTimeMax = map['end_time_max'];
    this.latitudeMin = map['latitude_min'];
    this.latitudeMax = map['latitude_max'];
    this.longitudeMin = map['longitude_min'];
    this.longitudeMax = map['longitude_max'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['slug'] = this.slug;
    map['label'] = this.label;
    map['title'] = this.title;
    map['launch'] = this.launch;
    map['body'] = this.body;
    map['scheduling'] = this.scheduling;
    map['start_time_min'] = this.startTimeMin;
    map['start_time_max'] = this.startTimeMax;
    map['end_time_min'] = this.endTimeMin;
    map['end_time_max'] = this.endTimeMax;
    map['latitude_min'] = this.latitudeMin;
    map['latitude_max'] = this.latitudeMax;
    map['longitude_min'] = this.longitudeMin;
    map['longitude_max'] = this.longitudeMax;
    return map;
  }
}
