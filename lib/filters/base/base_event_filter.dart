import 'package:playd_core/filters/base/base_filter.dart';

abstract class BaseEventFilter extends BaseFilter {
  String name;
  String description;
  int timestampMin;
  int timestampMax;
  double latitudeMin;
  double latitudeMax;
  double longitudeMin;
  double longitudeMax;

  BaseEventFilter() : super();

  BaseEventFilter.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.name = map['name'];
    this.description = map['description'];
    this.timestampMin = map['timestamp_min'];
    this.timestampMax = map['timestamp_max'];
    this.latitudeMin = map['latitude_min'];
    this.latitudeMax = map['latitude_max'];
    this.longitudeMin = map['longitude_min'];
    this.longitudeMax = map['longitude_max'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['name'] = this.name;
    map['description'] = this.description;
    map['timestamp_min'] = this.timestampMin;
    map['timestamp_max'] = this.timestampMin;
    map['latitude_min'] = this.latitudeMin;
    map['latitude_max'] = this.latitudeMax;
    map['longitude_min'] = this.longitudeMin;
    map['longitude_max'] = this.longitudeMax;
    return map;
  }
}
