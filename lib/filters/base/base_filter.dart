class BaseFilter {
  String uid;
  String identifier;
  String locale;
  String friendly;
  String category;
  String type;
  String subtype;
  int sortIndex;
  bool isVisible;
  bool isEnabled;
  bool isDeleted;
  int flags;
  String ownerId;
  String userId;
  String localId;
  String parentId;
  String ancestorId;
  String groupId;
  String externalId;
  int createTimeMin;
  int createTimeMax;
  int updateTimeMin;
  int updateTimeMax;
  int deleteTimeMin;
  int deleteTimeMax;

  BaseFilter();

  BaseFilter.fromMap(Map<String, dynamic> map) {
    this.uid = map['uid'];
    this.identifier = map['identifier'];
    this.locale = map['locale'];
    this.friendly = map['friendly'];
    this.category = map['category'];
    this.type = map['type'];
    this.subtype = map['subtype'];
    this.sortIndex = map['sort_index'];
    this.isVisible = (map['is_visible'] is int)
        ? ((map['is_visible'] == 1) ? true : false)
        : map['is_visible'];
    this.isEnabled = (map['is_enabled'] is int)
        ? ((map['is_enabled'] == 1) ? true : false)
        : map['is_enabled'];
    this.isDeleted = (map['is_deleted'] is int)
        ? ((map['is_deleted'] == 1) ? true : false)
        : map['is_deleted'];
    this.flags = map['flags'];
    this.ownerId = map['owner_id'];
    this.userId = map['user_id'];
    this.localId = map['local_id'];
    this.parentId = map['parent_id'];
    this.ancestorId = map['ancestor_id'];
    this.groupId = map['group_id'];
    this.externalId = map['external_id'];
    this.createTimeMin = map['create_time_min'];
    this.createTimeMin = map['create_time_max'];
    this.updateTimeMin = map['update_time_min'];
    this.updateTimeMax = map['update_time_max'];
    this.deleteTimeMin = map['delete_time_min'];
    this.deleteTimeMax = map['delete_time_max'];
  }

  Map<String, dynamic> toMap([bool boolToInt = false]) => {
        'uid': this.uid,
        'identifier': this.identifier,
        'locale': this.locale,
        'friendly': this.friendly,
        'category': this.category,
        'type': this.type,
        'subtype': this.subtype,
        'is_visible': (boolToInt) ? (this.isVisible ? 1 : 0) : this.isVisible,
        'is_enabled': (boolToInt) ? (this.isEnabled ? 1 : 0) : this.isEnabled,
        'is_deleted': (boolToInt) ? (this.isDeleted ? 1 : 0) : this.isDeleted,
        'sort_index': this.sortIndex,
        'flags': this.flags,
        'local_id': this.localId,
        'owner_id': this.ownerId,
        'user_id': this.userId,
        'parent_id': this.parentId,
        'ancestor_id': this.ancestorId,
        'group_id': this.groupId,
        'external_id': this.externalId,
        'create_time_min': this.createTimeMin,
        'create_time_max': this.createTimeMin,
        'update_time_min': this.updateTimeMin,
        'update_time_max': this.updateTimeMax,
        'delete_time_min': this.deleteTimeMin,
        'delete_time_max': this.deleteTimeMax,
      };
}
