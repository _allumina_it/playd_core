import 'package:playd_core/filters/base/base_filter.dart';

abstract class BaseItemFilter extends BaseFilter {
  double valueMin;
  double valueMax;
  double lastMin;
  double lastMax;
  double minMin;
  double minMax;
  double maxMin;
  double maxMax;
  double averageMin;
  double averageMax;
  double countMin;
  double countMax;
  double trendMin;
  double trendMax;
  int dayMin;
  int dayMax;
  int weekMin;
  int weekMax;
  int monthMin;
  int monthMax;
  int yearMin;
  int yearMax;
  int unitMin;
  int unitMax;
  int intervalMin;
  int intervalMax;

  BaseItemFilter() : super();

  BaseItemFilter.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.valueMin = map['value_min'];
    this.valueMax = map['value_max'];
    this.lastMin = map['last_min'];
    this.lastMax = map['last_max'];
    this.minMin = map['min_min'];
    this.minMax = map['min_max'];
    this.maxMin = map['max_min'];
    this.maxMax = map['max_max'];
    this.averageMin = map['average_min'];
    this.averageMax = map['average_max'];
    this.countMin = map['count_min'];
    this.countMax = map['count_max'];
    this.trendMin = map['trend_min'];
    this.trendMax = map['trend_max'];
    this.dayMin = map['day_min'];
    this.dayMax = map['day_max'];
    this.weekMin = map['week_min'];
    this.weekMax = map['week_max'];
    this.monthMin = map['month_min'];
    this.monthMax = map['month_max'];
    this.yearMin = map['year_min'];
    this.yearMax = map['year_max'];
    this.unitMin = map['unit_min'];
    this.unitMax = map['unit_max'];
    this.intervalMin = map['interval_min'];
    this.intervalMax = map['interval_max'];
  }

  @override
  Map<String, dynamic> toMap([bool boolToInt = false]) {
    Map<String, dynamic> map = super.toMap(boolToInt);
    map['value_min'] = this.valueMin;
    map['value_max'] = this.valueMax;
    map['last_min'] = this.lastMin;
    map['last_max'] = this.lastMax;
    map['min_min'] = this.minMin;
    map['min_max'] = this.minMax;
    map['max_min'] = this.maxMin;
    map['max_max'] = this.maxMax;
    map['average_min'] = this.averageMin;
    map['average_max'] = this.averageMax;
    map['count_min'] = this.countMin;
    map['count_max'] = this.countMax;
    map['trend_min'] = this.trendMin;
    map['trend_max'] = this.trendMax;
    map['day_min'] = this.dayMin;
    map['day_max'] = this.dayMax;
    map['week_min'] = this.weekMin;
    map['week_max'] = this.weekMax;
    map['month_min'] = this.monthMin;
    map['month_max'] = this.monthMax;
    map['year_min'] = this.yearMin;
    map['year_max'] = this.yearMax;
    map['unit_min'] = this.unitMin;
    map['unit_max'] = this.unitMax;
    map['interval_min'] = this.intervalMin;
    map['interval_max'] = this.intervalMax;
    return map;
  }
}
