import 'dart:io';
import 'package:meta/meta.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:playd_core/configuration/data_configuration.dart';
import 'package:playd_core/managers/base/base_manager.dart';
import 'package:sqflite/sqflite.dart';

class DataManager extends BaseManager {
  static const String TABLE_CONTENTS = 'contents';
  static const String TABLE_BINARIES = 'binaries';
  static const String TABLE_ITEMS = 'items';
  static const String TABLE_RAWS = 'raws';
  static const String TABLE_EVENTS = 'events';

  DataConfiguration configuration;

  static Database _database;

  static final DataManager _instance = DataManager._internal();

  factory DataManager() => _instance;

  void initialize(DataConfiguration configuration) {
    this.configuration = configuration;
  }

  // ------------------------------------------------------------------------------
  // COLUMNS
  // ------------------------------------------------------------------------------
  static final Map<String, String> _baseColumns = {
    'id': 'TEXT',
    'uid': 'TEXT',
    'identifier': 'TEXT',
    'locale': 'TEXT',
    'friendly': 'TEXT',
    'category': 'TEXT',
    'type': 'TEXT',
    'subtype': 'TEXT',
    'sort_index': 'INTEGER',
    'is_visible': 'INTEGER',
    'is_enabled': 'INTEGER',
    'is_deleted': 'INTEGER',
    'flags': 'INTEGER',
    'local_id': 'TEXT',
    'owner_id': 'TEXT',
    'user_id': 'TEXT',
    'parent_id': 'TEXT',
    'ancestor_id': 'TEXT',
    'group_id': 'TEXT',
    'external_id': 'TEXT',
    'version': 'INTEGER',
    'create_time': 'INTEGER',
    'update_time': 'INTEGER',
    'delete_time': 'INTEGER',
    'raw': 'TEXT',
    'dirty': 'INTEGER'
  };
  static final Map<String, String> _contentsColumns = {
    'title': 'TEXT',
    'slug': 'TEXT'
  };
  static final Map<String, String> _itemsColumns = {};
  static final Map<String, String> _rawsColumns = {};
  static final Map<String, String> _eventsColumns = {};
  static final Map<String, String> _binariesColumns = {};

  // ------------------------------------------------------------------------------
  // KEYS
  // ------------------------------------------------------------------------------
  static final List<String> _basePrimaryKeys = ['identifier', 'locale'];

  // ------------------------------------------------------------------------------
  // INDEXES
  // ------------------------------------------------------------------------------
  static final Map<String, bool> _baseIndexes = {
    'identifier': true,
    'locale': true,
    'owner_id': true,
    'user_id': true,
    'parent_id': true,
    'ancestor_id': true,
    'external_id': true
  };
  static final Map<String, bool> _contentsIndexes = {};
  static final Map<String, bool> _itemsIndexes = {};
  static final Map<String, bool> _binariesIndexes = {};
  static final Map<String, bool> _eventsIndexes = {};
  static final Map<String, bool> _rawsIndexes = {};

  DataManager._internal() {
    // init things inside this
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await open();
    return _database;
  }

  Future<Database> open() async {
    Directory _documentsDirectory = await getApplicationDocumentsDirectory();
    String _path = join(_documentsDirectory.path, configuration.databaseName);
    return await openDatabase(_path,
        version: configuration.databaseVersion, onUpgrade: _onUpgrade);
  }

  Future<void> close() async => _database.close();

  // ------------------------------------------------------------------------------
  // Support methods
  // ------------------------------------------------------------------------------
  String _trim(String string) {
    String temp = string.trim();
    if (temp.startsWith(',')) temp = temp.substring(1, temp.length);
    if (temp.endsWith(',')) temp = temp.substring(0, temp.length - 1);
    return temp.trim();
  }

  String _composeColumns(Map<String, String> columns) {
    String temp = '';
    columns.forEach((key, value) => {temp = temp + key + ' ' + value + ','});
    return _trim(temp);
  }

  String _composePrimaryKeys(List<String> primaryKeys) {
    String temp = '';
    primaryKeys.forEach((value) => {temp = temp + value + ','});
    return _trim(temp);
  }

  String _composeIndexes({String table, Map<String, bool> indexes}) {
    String _temp = '';
    indexes.forEach((key, value) => {
          _temp = ((_baseIndexes[key] != null)
              ? _temp +
                  ' CREATE INDEX idx_' +
                  key +
                  ' ON ' +
                  table +
                  '(' +
                  key +
                  ');'
              : _temp)
        });
    return _trim(_temp);
  }

  String _composeCreateTableQuery(
      {@required String tableName,
      @required Map<String, String> columns,
      @required List<String> primaryKeys,
      @required Map<String, bool> indexes}) {
    String temp = 'CREATE TABLE IF NOT EXISTS ';
    temp = temp + tableName;
    temp = temp + '(' + _composeColumns(columns);
    if (primaryKeys != null && primaryKeys.length > 0) {
      temp = ', PRIMARY KEY (' + _composePrimaryKeys(primaryKeys) + ')';
    }
    temp = temp + ");";
    if (indexes != null && indexes.length > 0) {
      temp = temp + _composeIndexes(table: tableName, indexes: indexes);
    }
    return _trim(temp);
  }

  Future<void> _onUpgrade(
      Database database, int oldVersion, int version) async {
    if (version > oldVersion) {
      // CONTENTS
      var contentsColumns = Map<String, String>();
      contentsColumns.addAll(_baseColumns);
      contentsColumns.addAll(_contentsColumns);
      var contentsIndexes = Map<String, bool>();
      contentsIndexes.addAll(_baseIndexes);
      contentsIndexes.addAll(_contentsIndexes);
      await database.execute(_composeCreateTableQuery(
          tableName: TABLE_CONTENTS,
          columns: contentsColumns,
          primaryKeys: _basePrimaryKeys,
          indexes: contentsIndexes));

      // ITEMS
      var itemsColumns = Map<String, String>();
      itemsColumns.addAll(_baseColumns);
      itemsColumns.addAll(_itemsColumns);
      var itemsIndexes = Map<String, bool>();
      itemsIndexes.addAll(_baseIndexes);
      itemsIndexes.addAll(_itemsIndexes);
      await database.execute(_composeCreateTableQuery(
          tableName: TABLE_ITEMS,
          columns: itemsColumns,
          primaryKeys: _basePrimaryKeys,
          indexes: itemsIndexes));

      // RAWS
      var rawsColumns = Map<String, String>();
      rawsColumns.addAll(_baseColumns);
      rawsColumns.addAll(_rawsColumns);
      var rawsIndexes = Map<String, bool>();
      rawsIndexes.addAll(_baseIndexes);
      rawsIndexes.addAll(_rawsIndexes);
      await database.execute(_composeCreateTableQuery(
          tableName: TABLE_RAWS,
          columns: rawsColumns,
          primaryKeys: _basePrimaryKeys,
          indexes: rawsIndexes));

      // EVENTS
      var eventsColumns = Map<String, String>();
      eventsColumns.addAll(_baseColumns);
      eventsColumns.addAll(_eventsColumns);
      var eventsIndexes = Map<String, bool>();
      eventsIndexes.addAll(_baseIndexes);
      eventsIndexes.addAll(_eventsIndexes);
      await database.execute(_composeCreateTableQuery(
          tableName: TABLE_EVENTS,
          columns: eventsColumns,
          primaryKeys: _basePrimaryKeys,
          indexes: eventsIndexes));

      // BINARIES
      var binariesColumns = Map<String, String>();
      binariesColumns.addAll(_baseColumns);
      binariesColumns.addAll(_binariesColumns);
      var binariesIndexes = Map<String, bool>();
      binariesIndexes.addAll(_baseIndexes);
      binariesIndexes.addAll(_binariesIndexes);
      await database.execute(_composeCreateTableQuery(
          tableName: TABLE_CONTENTS,
          columns: binariesColumns,
          primaryKeys: _basePrimaryKeys,
          indexes: binariesIndexes));
    }
  }
}
