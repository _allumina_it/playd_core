import 'dart:convert';
import 'dart:io';

import 'package:http/io_client.dart';
import 'package:playd_core/configuration/network_configuration.dart';
import 'package:playd_core/managers/base/base_manager.dart';
import 'package:playd_core/messages/int_response.dart';
import 'package:playd_core/messages/network_result.dart';

class NetworkManager extends BaseManager {
  NetworkConfiguration configuration;

  static final NetworkManager _instance = NetworkManager._internal();

  factory NetworkManager() => _instance;

  NetworkManager._internal() {
    // init things inside this
  }

  void initialize(NetworkConfiguration configuration) {
    this.configuration = configuration;
  }

  // ------------------------------------------------------------------------------------------
  // Utils
  // ------------------------------------------------------------------------------------------
  Future<bool> _networkAvailable() async {
    try {
      final result = await InternetAddress.lookup(configuration.lookupDomain);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

  Future<NetworkResult<IntResponse>> time() async {
    bool networkAvailable = await _networkAvailable();
    if (networkAvailable) {
      var uri = configuration.getUri('/system/utils/time');
      var response;
      try {
        response =
            await _initClient().get(uri, headers: _composeRequestHeaders());
        if (response.statusCode == 200) {
          IntResponse output = IntResponse.fromMap(json.decode(response.body));
          return NetworkResult<IntResponse>(
              result: NetworkResults.SUCCESS, data: output);
        }
      } on Exception catch (e) {
        var result = NetworkResult<IntResponse>(
            result: NetworkResults.INTERNAL_EXCEPTION, data: null);
        result.exception = e;
        return result;
      }
      return NetworkResult<IntResponse>(
          result: NetworkResults.FAILURE, data: response);
    }
    return NetworkResult<IntResponse>(
        result: NetworkResults.NETWORK_UNREACHABLE, data: null);
  }

  Map<String, String> _composeRequestHeaders(
      {bool addClient = false,
      String grantType = '',
      String token = '',
      String accept = 'application/json'}) {
    Map<String, String> headers = new Map<String, String>();
    if (accept != null) headers['Accept'] = accept;

    if (configuration.applicationIdentifier.length > 0)
      headers['application_id'] = configuration.applicationIdentifier;

    if (addClient) {
      headers['client_id'] = configuration.clientId;
      headers['client_secret'] = configuration.clientSecret;
    }
    if (grantType.length > 0) {
      headers['grant_type'] = grantType;
    }
    if (token.length > 0) {
      headers['Authorization'] = 'Bearer $token';
    }
    return headers;
  }

  Map<String, String> _composeRequestData(
      {bool addClient = false, String grantType = ''}) {
    Map<String, String> data = new Map<String, String>();
    if (addClient) {
      data['client_id'] = configuration.clientId;
      data['client_secret'] = configuration.clientSecret;
    }
    if (grantType.length > 0) {
      data['grant_type'] = grantType;
    }
    return data;
  }

  IOClient _initClient() {
    HttpClient httpClient = HttpClient();
    assert(() {
      httpClient.badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

      return true;
    }());
    IOClient ioClient = new IOClient(httpClient);
    return ioClient;
  }
}
