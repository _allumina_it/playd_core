import 'package:playd_core/managers/base/base_manager.dart';

class SecureStorageManager extends BaseManager {
  static final SecureStorageManager _instance =
      SecureStorageManager._internal();

  factory SecureStorageManager() => _instance;

  SecureStorageManager._internal() {
    // init things inside this
  }
}
