abstract class BaseRequest {
  dynamic data;
  int count;
  int page;
  int size;
  int timestamp;

  BaseRequest();

  Map<String, dynamic> toMap() => {
        'data': this.data,
        'count': this.count,
        'page': this.page,
        'size': this.size,
        'timestamp': this.timestamp
      };
}
