abstract class BaseResponse {
  int status;
  int page;
  int count;
  String error;
  String message;

  BaseResponse();

  BaseResponse.fromMap(Map<String, dynamic> map) {
    this.status = map['status'];
    this.page = map['page'];
    this.count = map['count'];
    this.error = map['error'];
  }
}
