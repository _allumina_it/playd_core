import 'package:playd_core/messages/base/base_response.dart';

class IntResponse extends BaseResponse {
  int data;

  IntResponse.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    this.data = map['data'];
  }
}
