import 'package:meta/meta.dart';
import 'package:playd_core/messages/base/base_result.dart';

enum NetworkResults {
  SUCCESS,
  FAILURE,
  INTERNAL_EXCEPTION,
  UNAUTHORIZED,
  NETWORK_UNREACHABLE
}

class NetworkResult<T> extends BaseResult {
  NetworkResults result;
  T data;

  NetworkResult({@required this.result, @required this.data}) : super();
}
