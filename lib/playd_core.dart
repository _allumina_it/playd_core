import 'dart:async';

import 'package:flutter/services.dart';

class PlaydCore {
  static const MethodChannel _channel =
      const MethodChannel('playd_core');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
