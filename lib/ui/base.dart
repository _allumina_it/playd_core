import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class BaseStatelessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return null;
  }

  void push({@required BuildContext context, @required String routeName}) {
    Navigator.of(context).pushNamed(routeName);
  }

  void pushAndForget(
      {@required BuildContext context, @required String routeName}) {
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  void pushModal({@required BuildContext context, @required Widget page}) {
    Navigator.of(context).push(
        CupertinoPageRoute(fullscreenDialog: true, builder: (context) => page));
  }

  void back({@required BuildContext context}) {
    Navigator.of(context).pop();
  }

  void pop({@required BuildContext context}) {
    Navigator.of(context).pop();
  }

  void popToRoot({@required BuildContext context, @required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }

  void popToStart(
      {@required BuildContext context, @required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }
}

abstract class BaseStatefulPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return null;
  }

  Future<void> showAlert(
      {@required BuildContext context,
      @required String title,
      @required String content,
      @required String buttonText,
      @required Function buttonAction,
      bool barrierDismissible = true}) {
    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(buttonText),
              onPressed: buttonAction,
            ),
          ],
        );
      },
    );
  }

  Future<void> launchURL({@required String url}) async {
    if (await canLaunch(url)) {
      return await launch(url);
    }
  }

  Future<void> launchFile({@required String url}) async {
    if (await canLaunch(url)) {
      return await launch(url, forceSafariVC: false, forceWebView: false);
    }
  }

  void push({@required BuildContext context, @required String routeName}) {
    Navigator.of(context).pushNamed(routeName);
  }

  void pushAndForget(
      {@required BuildContext context, @required String routeName}) {
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  void pushModal({@required BuildContext context, @required Widget page}) {
    Navigator.of(context).push(
        CupertinoPageRoute(fullscreenDialog: true, builder: (context) => page));
  }

  void back({@required BuildContext context}) {
    Navigator.of(context).pop();
  }

  void pop({@required BuildContext context}) {
    Navigator.of(context).pop();
  }

  void popToRoot({@required BuildContext context, @required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }

  void popToStart(
      {@required BuildContext context, @required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }
}

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  bool isLoading = false;
  bool firstLoad = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!mounted) return;
    // Locale locale = Localizations.localeOf(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> showAlert(
      {@required BuildContext context,
      @required String title,
      @required String content,
      @required String buttonText,
      @required Function buttonAction,
      bool barrierDismissible = true}) {
    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(buttonText),
              onPressed: buttonAction,
            ),
          ],
        );
      },
    );
  }

  Future<void> showConfirm(
      {@required BuildContext context,
      @required String title,
      @required String content,
      @required String confirmButtonText,
      @required Function confirmButtonAction,
      @required String cancelButtonText,
      @required Function cancelButtonAction,
      bool barrierDismissible = true}) {
    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(confirmButtonText),
              onPressed: confirmButtonAction,
            ),
            FlatButton(
              child: Text(cancelButtonText),
              onPressed: cancelButtonAction,
            ),
          ],
        );
      },
    );
  }

  Future<void> launchURL({@required String url}) async {
    if (await canLaunch(url)) {
      return await launch(url);
    }
  }

  Future<void> launchFile({@required String url}) async {
    if (await canLaunch(url)) {
      return await launch(url, forceSafariVC: false, forceWebView: false);
    }
  }

  void back() {
    Navigator.of(context).pop();
  }

  void push({@required String routeName}) {
    Navigator.of(context).pushNamed(routeName);
  }

  void pushAndForget({@required String routeName}) {
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  void pushModal({@required Widget page}) {
    Navigator.of(context).push(
        CupertinoPageRoute(fullscreenDialog: true, builder: (context) => page));
  }

  void pop() {
    Navigator.of(context).pop();
  }

  void popToRoot({@required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }

  void popToStart({@required String routeName}) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(routeName, (Route<dynamic> route) => false);
  }
}
