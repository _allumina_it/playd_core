import 'package:flutter/material.dart';

class Progress extends StatelessWidget {
  static const DEFAULT_PADDING_TOP = 24.0;
  static const DEFAULT_PADDING_LEFT = 16.0;
  static const DEFAULT_PADDING_RIGHT = 16.0;
  static const DEFAULT_PADDING_BOTTOM = 24.0;
  static const DEFAULT_TEXT_SIZE = 14.0;
  static const DEFAULT_BORDER_RADIUS = 10.0;

  final Color progressColor;
  final Color backgroundColor;
  final double borderRadius;
  final String text;
  final Color textColor;
  final double textSize;
  final EdgeInsets padding;

  Progress(
      {Key key,
      this.text,
      this.progressColor,
      this.backgroundColor,
      this.borderRadius = DEFAULT_BORDER_RADIUS,
      this.textSize = DEFAULT_TEXT_SIZE,
      this.textColor = Colors.black,
      this.padding = const EdgeInsets.only(
          top: DEFAULT_PADDING_TOP,
          left: DEFAULT_PADDING_LEFT,
          bottom: DEFAULT_PADDING_BOTTOM,
          right: DEFAULT_PADDING_RIGHT)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
        decoration: BoxDecoration(color: Colors.transparent),
        child: Center(
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(this.borderRadius),
                  color: this.backgroundColor,
                ),
                child: Padding(
                    padding: this.padding,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                this.progressColor)),
                        Padding(
                            padding: EdgeInsets.only(top: 16.0, bottom: 0.0),
                            child: Text(this.text,
                                style: TextStyle(
                                    fontSize: this.textSize,
                                    color: this.textColor)))
                      ],
                    )))));
  }
}
