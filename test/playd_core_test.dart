import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:playd_core/playd_core.dart';

void main() {
  const MethodChannel channel = MethodChannel('playd_core');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await PlaydCore.platformVersion, '42');
  });
}
